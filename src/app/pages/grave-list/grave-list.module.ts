import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule} from '@angular/forms';

import { GraveListPage } from './grave-list';
import { GraveListPageRoutingModule } from './grave-list-routing.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    GraveListPageRoutingModule
  ],
  declarations: [GraveListPage],
})
export class GraveListModule {}
