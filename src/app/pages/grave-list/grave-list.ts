import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { ActionSheetController } from '@ionic/angular';

import { ConferenceData } from '../../providers/conference-data';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'page-grave-list',
  templateUrl: 'grave-list.html',
  styleUrls: ['./grave-list.scss'],
})
export class GraveListPage {
  grave: any[] = [];
  filter: { [index: string]: any; }={abteilung:'',reihe:'',stelle:''};
  facet: { [index: string]: any[]; }={abteilung:[],reihe:[],stelle:[]};
  hideFilter:boolean = false;

  constructor(
    public actionSheetCtrl: ActionSheetController,
    public confData: ConferenceData,
    public inAppBrowser: InAppBrowser,
    public router: Router
  ) {}


  filterGo(ev:any){
    
    this.confData.getgrave().subscribe((grave: any[]) => {            
               
        this.grave=grave.filter((graveItem: any) =>{                                       
          let result=true;          
          for (let key in this.filter) {                    
            if(this.filter[key]){    
              result=result&&(graveItem[key]==this.filter[key]);              
            }
          }
          return result;
        });      
    });
    
  }

  hideShowFilter(){    
    this.hideFilter=!this.hideFilter;
  
  }

  ionViewDidEnter() {
    this.confData.getgrave().subscribe((grave: any[]) => {      
      
      for (let entry of grave) {        
          for (let key in this.facet) {                        
            if(this.facet[key].indexOf(entry[key])==-1)  {
                this.facet[key].push(entry[key]);
            }            
          }  
      }

      console.log(this.facet);
      this.grave = grave;
    });
  }


  searchForGrave(ev: any) {
    let val = ev.target.value;         
    this.confData.getgrave().subscribe((grave: any[]) => {            
            
      if(val!=''){        
        this.grave=grave.filter((graveItem: any) =>{                
            return graveItem.name.toLowerCase().indexOf(val.toLowerCase())!=-1;
        });      
            
        }else{
            this.grave=grave;
        }
      
    });
    }

  }



 



