import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs-page';
import { TabsPageRoutingModule } from './tabs-page-routing.module';
import { GraveDetailModule } from '../grave-detail/grave-detail.module';
import { GraveListModule } from '../grave-list/grave-list.module';

@NgModule({
  imports: [    
    CommonModule,
    IonicModule,
    GraveDetailModule,
    GraveListModule,
    TabsPageRoutingModule
  ],
  declarations: [
    TabsPage,
  ]
})
export class TabsModule { }
