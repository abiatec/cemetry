import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs-page';
import { SchedulePage } from '../schedule/schedule';


const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      
      {
        path: '',
        redirectTo: 'grave',
        pathMatch: 'full'
      },
      
      {
        path: 'grave',
        children: [
          {
            path: '',
            loadChildren: '../grave-list/grave-list.module#GraveListModule'
          },          
          {
            path: 'grave-details/:graveId',
            loadChildren: '../grave-detail/grave-detail.module#GraveDetailModule'
          }
        ]
      }
      
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }

