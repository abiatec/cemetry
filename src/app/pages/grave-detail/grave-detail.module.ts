import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GraveDetailPage } from './grave-detail';
import { GraveDetailPageRoutingModule } from './grave-detail-routing.module';
import { IonicModule } from '@ionic/angular';

import { Camera } from '@ionic-native/camera/ngx';

@NgModule({
  imports: [    
    CommonModule,
    IonicModule,
    GraveDetailPageRoutingModule
  ],
  declarations: [
    GraveDetailPage,
  ],
  providers: [
    Camera
  ]
})
export class GraveDetailModule { }
