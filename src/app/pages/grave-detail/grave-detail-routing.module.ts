import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GraveDetailPage } from './grave-detail';

const routes: Routes = [
  {
    path: '',
    component: GraveDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GraveDetailPageRoutingModule { }
