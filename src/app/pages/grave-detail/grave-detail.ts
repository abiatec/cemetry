import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConferenceData } from '../../providers/conference-data';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';



@Component({
  selector: 'page-grave-detail',
  templateUrl: 'grave-detail.html',
  styleUrls: ['./grave-detail.scss'],
})
export class GraveDetailPage {
  grave: any;

  optionsCamera: CameraOptions;
  clickedImagePath:any;
  constructor(
    private dataProvider: ConferenceData,
    private router: Router,
    private route: ActivatedRoute,
    private camera: Camera
  ) {
    
    
    this.optionsCamera={
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
  }
  

  }

  getCamera(ev:any){

    this.camera.getPicture(this.optionsCamera).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      this.grave.picsAttached.push('data:image/jpeg;base64,' + imageData);      
     }, (err) => {
      console.log(err);
     });


  }



  ionViewWillEnter() {
    this.dataProvider.load().subscribe((data: any) => {
      const graveId = this.route.snapshot.paramMap.get('graveId');
      if (data && data.grave) {
        for (const grave of data.grave) {
          if (grave && grave.id === graveId) {
            this.grave = grave;
            break;
          }
        }
      }
    });
  }
}
